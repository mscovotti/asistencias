<?php
$module = (empty($_GET['module'])) ? 'alumno' : $_GET['module'];
?>
<!DOCTYPE html>
<html>
<head>
  <title>Asistencias</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>

  <?php include('parts/navbar.php'); ?>

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
<?php
// Esto es el dispatcher de la aplicación
// Primero se construye la ruta al archivo controlador del módulo
$controller_file = "modules/$module/$module.php";

if(file_exists($controller_file)){
  require_once($controller_file);
  //require_once('modules/curso/curso.php');
  // Se ejecuta la función definida en el módulo que tiene el mismo nombre que el módulo que se incluyó
  $module();
}
?>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
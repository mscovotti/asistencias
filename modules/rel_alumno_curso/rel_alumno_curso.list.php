<h1><?php echo $curso['nombre'] ?></h1>
<form action="?module=rel_alumno_curso&action=asignar" method="POST">
  <input type="hidden" name="id_curso" value="<?php echo $curso['id'] ?>">
  <select name="id_alumno">
    <option value="">Seleccione un alumno</option>
    <?php
    while($row = $stmt_alumnos->fetch()){
      echo '<option value="', $row['id'], '">', $row['apellido'], ', ', $row['nombre'], '</option>';
    }
    ?>
  </select>
  <input type="submit" value="Agregar" />
</form>

<table class="table">
  <thead>
    <tr>
      <th>Alumnos</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
<?php
  while($row = $stmt_alumnos_asignados->fetch()){
    echo '<tr>';
    echo '<td>', $row['apellido'], ', ', $row['nombre'], '</td>';
    echo '<td><a href="?module=rel_alumno_curso&action=eliminar&id_curso=', $curso['id'], '&id_alumno=', $row['id_alumno'], '">[Eliminar]</a></td>';
    echo '</tr>';
  }
?>    
  </tbody>
</table>
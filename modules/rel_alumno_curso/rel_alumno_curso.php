<?php
require_once('lib/db_connection.php');
require_once('modules/curso/curso.php');
require_once('modules/alumno/alumno.php');

function rel_alumno_curso(){
  $action = (empty($_GET['action'])) ? 'listar' : $_GET['action'];

  switch ($action) {
    case 'asignar':
      //print_r($_POST); // $_POST['id_alumno'];
      insertar_alumno_curso($_POST);
      header("location: ?module=rel_alumno_curso&id_curso=".$_POST['id_curso']);
      break;
    case 'eliminar':
      eliminar_alumno_curso($_GET);
      header("location: ?module=rel_alumno_curso&id_curso=".$_GET['id_curso']);
      break;
    case 'listar':
    default:
      $curso = obtener_curso($_GET['id_curso']);
      $stmt_alumnos = obtener_alumnos();
      $stmt_alumnos_asignados = obtener_alumnos_cursos($_GET['id_curso']);
      //print_r($curso);
      include('rel_alumno_curso.list.php');
      break;
  }
}

function insertar_alumno_curso($data){
  $dbh = getConnection();

  $stmt = $dbh->prepare("INSERT INTO alumnos_cursos (id_alumno, id_curso) VALUES (:id_alumno, :id_curso)");
  $stmt->bindParam(':id_alumno', $data['id_alumno']);
  $stmt->bindParam(':id_curso', $data['id_curso']);

  $stmt->execute();
}

function obtener_alumnos_cursos($id_curso){
  $dbh = getConnection();

  $stmt = $dbh->prepare("SELECT ac.id_alumno,
                                ac.id_curso,
                                a.nombre,
                                a.apellido
                           FROM alumnos_cursos AS ac 
                           JOIN alumnos AS a
                             ON a.id = ac.id_alumno
                          WHERE ac.id_curso = :id_curso");
  $stmt->bindParam(':id_curso', $id_curso);
  $stmt->execute();
  
  return $stmt;
}

function eliminar_alumno_curso($data){
  $dbh = getConnection();

  $stmt = $dbh->prepare("DELETE FROM alumnos_cursos WHERE id_alumno = :id_alumno AND id_curso = :id_curso");
  $stmt->bindParam(':id_alumno', $data['id_alumno']);
  $stmt->bindParam(':id_curso', $data['id_curso']);

  $stmt->execute();
}
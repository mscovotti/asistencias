<a class="btn btn-primary" href="?module=alumno&action=nuevo">Nuevo alumno</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Apellido</th>
      <th>Cédula</th>
      <th>Teléfono</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
<?php  
while($row = $stmt->fetch()){
	//print_r($row);
	echo '<tr>';
	echo '<td>',$row['nombre'],'</td>';
	echo '<td>',$row['apellido'],'</td>';
	echo '<td>',$row['cedula'],'</td>';
	echo '<td>',$row['telefono'],'</td>';
  echo '<td><a href="?module=alumno&action=editar&id=',$row['id'],'">[Editar]</a> <a href="?module=alumno&action=eliminar&id=',$row['id'],'">[Eliminar]</a></td>';
	echo '</tr>';
}
?>
  </tbody>
</table>
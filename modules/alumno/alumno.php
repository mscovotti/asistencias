<?php
require_once('lib/db_connection.php');

function alumno(){

	$action = (empty($_GET['action'])) ? 'listar' : $_GET['action'];

	switch($action){
		case 'nuevo':
			$form_action = 'insertar';
			$alumno = array('id' => 			null, 
											'nombre' => 	null, 
											'apellido' => null, 
											'cedula' => 	null, 
											'telefono' => null);
			include('alumno.form.php');
			break;
		case 'insertar':
			insertar_alumno($_POST['alumno']);
			header("location:?module=alumno");
			break;
		case 'editar':
			$alumno = obtener_alumno($_GET['id']);
			$form_action = 'modificar';
			include('alumno.form.php');
			break;
		case 'modificar':
			modificar_alumno($_POST['alumno']);
			header("location:?module=alumno");
			break;
		case 'eliminar':
			eliminar_alumno($_GET['id']);
			header("location:?module=alumno");
			break;
		case 'listar':
		default:
			$stmt = obtener_alumnos();
			include('alumnos.list.php');
			break;
	};
}

function obtener_alumnos(){
  $dbh = getConnection();

	$stmt = $dbh->prepare("SELECT * FROM alumnos");
	$stmt->execute();

	return $stmt;
}

function obtener_alumno($id){
  $dbh = getConnection();

	$stmt = $dbh->prepare("SELECT * FROM alumnos WHERE id = :id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	return $stmt->fetch();
}

function insertar_alumno($alumno){
  $dbh = getConnection();
  $err = array();

  if(empty($alumno['nombre'])){
  	$err[] = 'El nombre no puede ser nulo';
  }

	$stmt = $dbh->prepare("INSERT INTO alumnos (nombre, apellido, cedula, telefono) VALUES (:nombre, :apellido, :cedula, :telefono)");

	$stmt->bindParam(':nombre', 	$alumno['nombre']);
	$stmt->bindParam(':apellido', $alumno['apellido']);
	$stmt->bindParam(':cedula', 	$alumno['cedula']);
	$stmt->bindParam(':telefono', $alumno['telefono']);

	$stmt->execute();

	return $err;
}

function modificar_alumno($alumno){
	$dbh = getConnection();

	$stmt = $dbh->prepare("UPDATE alumnos SET nombre = :nombre, apellido = :apellido, cedula = :cedula, telefono = :telefono WHERE id = :id");
	$stmt->bindParam(':id', 			$alumno['id']);
	$stmt->bindParam(':nombre', 	$alumno['nombre']);
	$stmt->bindParam(':apellido', $alumno['apellido']);
	$stmt->bindParam(':cedula', 	$alumno['cedula']);
	$stmt->bindParam(':telefono', $alumno['telefono']);

	$stmt->execute();
}

function eliminar_alumno($id){
  $dbh = getConnection();

	$stmt = $dbh->prepare("DELETE FROM alumnos WHERE id = :id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
}
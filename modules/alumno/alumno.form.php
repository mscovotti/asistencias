<h1><?php echo (($form_action == 'insertar') ? 'Nuevo alumno' : 'Editar alumno') ?></h1>
<form action="?module=alumno&action=<?php echo $form_action?>" method="POST">
  <input type="hidden" name="alumno[id]" value="<?php echo $alumno['id']?>" /> 

  <div class="form-group">
  	<label for="nombre">Nombre</label>
  	<input type="text" class="form-control" name="alumno[nombre]" id="nombre" value="<?php echo $alumno['nombre']?>" placeholder="Nombre" />
  </div>

  <div class="form-group">
  	<label for="apellido">Apellido</label>
  	<input type="text" class="form-control" name="alumno[apellido]" id="apellido" value="<?php echo $alumno['apellido']?>" placeholder="Apellido" />
  </div>

	<div class="form-group">
    <label for="cedula">Cédula</label>
  	<input type="text" class="form-control" name="alumno[cedula]" id="cedula" value="<?php echo $alumno['cedula']?>" placeholder="Cédula" />
  </div>

  <div class="form-group">
  	<label for="telefono">Teléfono</label>
  	<input type="text" class="form-control" name="alumno[telefono]" id="telefono" value="<?php echo $alumno['telefono']?>" placeholder="Teléfono" />
  </div>

	<input type="submit" class="btn btn-primary" name="submit" value="Enviar" />
</form>
<?php
require_once('lib/db_connection.php');

function curso(){

	$action = (empty($_GET['action'])) ? 'listar' : $_GET['action'];

	switch($action){
		case 'nuevo':
			$form_action = 'insertar';
			$curso = array('id' => 			null, 
										 'nombre' => 	null, 
										 'periodo' => null);
			include('curso.form.php');
			break;
		case 'insertar':
			insertar_curso($_POST['curso']);
			header("location:?module=curso");
			break;
		case 'editar':
			$curso = obtener_curso($_GET['id']);
			$form_action = 'modificar';
			include('curso.form.php');
			break;
		case 'modificar':
			modificar_curso($_POST['curso']);
			header("location:?module=curso");
			break;
		case 'eliminar':
			eliminar_curso($_GET['id']);
			header("location:?module=curso");
			break;
		case 'listar':
		default:
			$stmt = obtener_cursos();
			include('cursos.list.php');
			break;
	};
}

function listar_curso(){
	echo "Esto increiblemente funciona!";
}



function obtener_cursos(){
  $dbh = getConnection();

	$stmt = $dbh->prepare("SELECT * FROM cursos");
	$stmt->execute();
	
	return $stmt;
}

function obtener_curso($id){
  $dbh = getConnection();

	$stmt = $dbh->prepare("SELECT * FROM cursos WHERE id = :id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	return $stmt->fetch();
}

function insertar_curso($curso){
  $dbh = getConnection();
  $err = array();

  if(empty($curso['nombre'])){
  	$err[] = 'El nombre no puede ser nulo';
  }

	$stmt = $dbh->prepare("INSERT INTO cursos (nombre, periodo) VALUES (:nombre, :periodo)");
	$stmt->bindParam(':nombre', 	$curso['nombre']);
	$stmt->bindParam(':periodo', $curso['periodo']);

	$stmt->execute();

	return $err;
}

function modificar_curso($curso){
	$dbh = getConnection();

	$stmt = $dbh->prepare("UPDATE cursos SET nombre = :nombre, periodo = :periodo WHERE id = :id");
	$stmt->bindParam(':id', 			$curso['id']);
	$stmt->bindParam(':nombre', 	$curso['nombre']);
	$stmt->bindParam(':periodo', $curso['periodo']);

	$stmt->execute();
}

function eliminar_curso($id){
  $dbh = getConnection();

	$stmt = $dbh->prepare("DELETE FROM cursos WHERE id = :id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
}

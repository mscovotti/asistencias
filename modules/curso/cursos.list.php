<a class="btn btn-primary" href="?module=curso&action=nuevo">Nuevo curso</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Periodo</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
<?php  
while($row = $stmt->fetch()){
	//print_r($row);
	echo '<tr>';
	echo '<td>',$row['nombre'],'</td>';
	echo '<td>',$row['periodo'],'</td>';
  echo '<td><a href="?module=rel_alumno_curso&id_curso=',$row['id'],'">[Alumnos]</a> <a href="?module=curso&action=editar&id=',$row['id'],'">[Editar]</a> <a href="?module=curso&action=eliminar&id=',$row['id'],'">[Eliminar]</a></td>';
	echo '</tr>';
}
?>
  </tbody>
</table>
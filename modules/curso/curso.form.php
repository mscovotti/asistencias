<h1><?php echo (($form_action == 'insertar') ? 'Nuevo curso' : 'Editar curso') ?></h1>
<form action="?module=curso&action=<?php echo $form_action?>" method="POST">
  <input type="hidden" name="curso[id]" value="<?php echo $curso['id']?>" /> 

	<div class="form-group">
  	<label for="nombre">Nombre</label>
		<input type="text" class="form-control" name="curso[nombre]" id="nombre" value="<?php echo $curso['nombre']?>" />
	</div>

	<div class="form-group">
  	<label for="periodo">Periodo</label>
		<input type="text" class="form-control" name="curso[periodo]" id="periodo" value="<?php echo $curso['periodo']?>" />
	</div>

	<input type="submit" class="btn btn-primary" name="submit" value="Enviar" />
</form>
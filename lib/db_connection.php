<?php

function getConnection(){
  static $dbh = null;

  if(empty($dbh)){
    try{
      $dbh = new PDO('mysql:host=localhost;dbname=asistencias', 'root', null, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      return $dbh;
    }catch(PDOException $e){
      echo "Error:", $e->getMessage();
      die();
    }
  }else{
    return $dbh;
  }
}